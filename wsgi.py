#!/usr/bin/env python2
# vim: fileencoding=utf-8:

from __future__ import absolute_import, print_function

from tsukkomi.utils import gevent_patch_all  # noqa

from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer
import bottle
import logging
import os

from tsukkomi import app

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
application = bottle.default_app()
bottle.BaseRequest.MEMFILE_MAX = 1024 * 1024


@bottle.hook('after_request')
def hook_enable_cors():
    bottle.response.headers['Access-Control-Allow-Origin'] = '*'


if __name__ == '__main__':
    host = "127.0.0.1"
    port = 8051
    os.chdir(os.path.dirname(os.path.realpath(__file__)))  # fix template path
    logger.info("Starting WSGI server at %s:%d", host, port)
    # bottle.run(host=host, port=port, debug=True)
    server = WSGIServer((host, port), application,
                        handler_class=WebSocketHandler)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        logger.info("INT received, exiting")
