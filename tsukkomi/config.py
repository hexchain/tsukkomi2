#!/usr/bin/env python2
# vim: fileencoding=utf-8:

from __future__ import absolute_import, print_function, unicode_literals

import logging

from .dbconfig import database
from .utils.singleton import Singleton
from .utils.helpers import recursive_set

logger = logging.getLogger(__name__)


class _config(Singleton):
    def __init__(self, db, coll_name="config"):
        self._db = db
        self._coll = self._db[coll_name]
        self.config = {}
        self.reload()

    def reload(self):
        self.config = self._coll.find_one({'name': 'config'})

    def partial_reload(self, path):
        new_config = self._coll.find_one(
            {'name': 'config'},
            projection={path: True, '_id': False})

        if not new_config:
            return False

        recursive_set(self.config, new_config, overwrite=True)

    def set_config(self, new_config):
        self._coll.update_one({'name': 'config'},
                              {'$set': new_config},
                              upsert=True)
        self.reload()

    def get_config(self):
        return self.config


_configobj = _config(database)
config = _configobj.get_config()
