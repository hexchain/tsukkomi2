# vim: fileencoding=utf-8:

from __future__ import absolute_import, print_function, unicode_literals

from bottle import (request, route, abort)
from geventwebsocket import WebSocketError
from bson import json_util
import logging
import os

from .dbconfig import database
from .backend import tsukkomi
from .endpoint import wechat   # noqa
from .endpoint import website  # noqa
from .config import config     # noqa
from .utils.enum import ControlResult

logger = logging.getLogger(__name__)
tsukkomi_back = tsukkomi.TsukkomiBackend(database)


@route('/api/stream')
def api_stream():
    ws = request.environ.get('wsgi.websocket')
    if not ws:
        abort(400, "Expecting a WebSocket request")

    first_msg = {
        'type': 'control',
        'reason': ControlResult.SUCCESS.value
    }

    try:
        msg = ws.receive()
        msg = json_util.loads(msg)
        if not msg['key'] or msg['key'] != config['api']['key']:
            first_msg['reason'] = ControlResult.WRONG_KEY.value
            ws.close(message=json_util.dumps(first_msg))
            return
    except:
        first_msg['reason'] = ControlResult.NO_KEY.value
        ws.close(message=json_util.dumps(first_msg))

    first_msg['code'] = config['field']['code']
    try:
        ws.send(json_util.dumps(first_msg))
    except:
        ws.close()
        return

    stream_cursor = tsukkomi_back.stream_cursor()

    try:
        while True:
            while stream_cursor.alive:
                ws.send_frame(os.urandom(16), ws.OPCODE_PING)
                for item in stream_cursor:
                    # remove unneeded fields
                    map(item.pop, ('_id', 'ts', 'user'))
                    item['type'] = 'comment'
                    ws.send(json_util.dumps(item))
    except WebSocketError as e:
        logger.warning("Websocket Error: %s", repr(e))
    finally:
        stream_cursor.close()
