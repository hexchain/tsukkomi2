# vim: fileencoding=utf-8:

from __future__ import print_function, unicode_literals

from hashlib import sha1


# WeChat
def wechat_authenticate(signature, timestamp, nonce, token):
    l = [timestamp, nonce, token]
    if sha1(''.join(sorted(l))).hexdigest() == signature:
        return True
    return False


# XML
def value_of(tree, xpath):
    return tree.xpath(xpath)[0].text
