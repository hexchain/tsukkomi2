# vim: fileencoding=utf-8:

from __future__ import absolute_import, print_function, unicode_literals

from enum import Enum, unique


@unique
class TsukkomiResult(Enum):
    SUCCESS = 0
    HOLDOFF = 1
    BLACKLIST = 2
    EMPTY = 3
    NEEDAUTH = 4
    TOOLONG = 5


@unique
class ControlResult(Enum):
    SUCCESS = 0
    WRONG_KEY = 1
    NO_KEY = 2
