# vim: fileencoding=utf-8:

from __future__ import absolute_import, print_function, unicode_literals

import datetime
import time
import random
import string
import uuid

try:
    import regex as re
except ImportError:
    import re


def isRGBcolor(text):
    return bool(re.match(r"^#?[0-9a-fA-F]{6}$", text))


def random_userid():
    return uuid.uuid4().hex


def random_code(length):
    return ''.join([random.choice(string.ascii_lowercase + string.digits)
                    for _ in range(length)])


def utc_datetime():
    return datetime.datetime.utcnow()


def utc_timestamp():
    return time.strftime('%s', time.gmtime())


def recursive_set(target, source, overwrite=False):
    for key in source:
        if key not in target:
            target[key] = source[key]
        elif not isinstance(target[key], type(source[key])):
            # type conflict, ignore this
            continue
        else:
            if isinstance(source[key], dict):  # both dict, recurse
                recursive_set(source[key], target[key], overwrite)
            else:
                if overwrite:
                    target[key] = source[key]
