# vim: fileencoding=utf-8:

from __future__ import absolute_import, unicode_literals

from pymongo.cursor import CursorType
from pymongo.collection import ReturnDocument
import logging
import pymongo

from ..config import config
from ..utils.singleton import Singleton
from ..utils.helpers import utc_datetime

logger = logging.getLogger(__name__)


class TsukkomiBackend(Singleton):

    _coll_size = 2 ** 24

    class Sequence:
        def __init__(self, db, coll_name="_sequence"):
            self._db = db
            self._coll = self._db[coll_name]

        def cur(self, name="comment"):
            doc = self._coll.find_one({'_id': name})
            if not doc:
                return 0
            return doc['value']

        def next(self, name="comment", inc=1):
            doc = self._coll.find_one_and_update(
                {'_id': name},
                {'$inc': {'value': inc}},
                upsert=True,
                return_document=ReturnDocument.AFTER)
            return doc['value']

    def __init__(self, db, coll_name="comments"):
        self._db = db
        self._coll_name = coll_name
        coll_options = {
            'size': self._coll_size,
            'capped': True,
            'autoIndexId': False
        }
        try:
            self._coll = self._db.create_collection(coll_name, **coll_options)
        except pymongo.errors.CollectionInvalid:
            # exists a collection with same name, check if it's what we want
            if self._db[coll_name].options() == coll_options:
                self._coll = self._db[coll_name]
            else:
                raise

        self._sequence = self.Sequence(self._db)
        self.blacklist = config['comment']['blacklist']
        self._msgid_cache = []
        self._msgid_cache_size = 10

    def check_blacklist(self, text, reload=False):
        if reload:
            config.partial_reload('comment.blacklist')
            self.blacklist = config['comment']['blacklist']
        for word in self.blacklist:
            if word in text:
                return True
        return False

    def publish(self, source, username, color, text, msgid=None):
        doc = {
            'ts': self._sequence.next(),
            'source': source,
            'user': username,
            'color': color,
            'text': text,
            'birth': utc_datetime(),
            'blacklisted': self.check_blacklist(text)
        }
        if msgid:
            if msgid in self._msgid_cache:
                logging.info("Received %s twice!", msgid)
                return doc['blacklisted']
            else:
                self._msgid_cache.insert(0, msgid)
                if len(self._msgid_cache) > self._msgid_cache_size:
                    self._msgid_cache = \
                        self._msgid_cache[:self._msgid_cache_size]
        try:
            self._coll.insert_one(doc)
        except Exception as e:
            logger.error("Caught exception: %s", e)
        return doc['blacklisted']

    def get_cursor(self, last_id=None):
        if not last_id:
            last_id = self._sequence.cur()

        cursor = self._coll.find(
            {'ts': {'$gt': last_id}, 'blacklisted': False},
            cursor_type=CursorType.TAILABLE_AWAIT,
            oplog_replay=True
        ).hint([('$natural', 1)])
        return cursor

    def stream_cursor(self):
        last_id = self._sequence.cur()
        cursor = self.get_cursor(last_id)
        return cursor
