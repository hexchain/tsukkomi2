# vim: fileencoding=utf-8:

from __future__ import absolute_import, print_function, unicode_literals

from pymongo.collection import ReturnDocument
from pymongo.errors import DuplicateKeyError
import logging

from . import tsukkomi
from ..utils.singleton import Singleton
from ..utils.helpers import utc_datetime, random_userid, isRGBcolor
from ..utils.enum import TsukkomiResult
from ..dbconfig import database
from ..config import config

logger = logging.getLogger(__name__)
tsukkomi_back = tsukkomi.TsukkomiBackend(database)


class BaseUserData(Singleton):

    _ident = "base"

    def __init__(self, db, coll_name="userdata"):
        self._db = db
        self._coll = self._db[coll_name]
        self._cache = {}

    def register(self, *args, **kwargs):
        return True

    def verify(self, userid):
        result = self._coll.find_one({'_id': userid})
        if not result:
            return False
        return result['verified']

    def holdoff(self, userid, current):
        result = self._coll.find_one(
            {'_id': userid},
            projection={'holdoff': True, 'lastseen': True})
        if 'lastseen' in result:
            lastseen = result['lastseen']
        else:
            return False
        if 'holdoff' in result:
            holdoff = result['holdoff']
        else:
            holdoff = config['user']['holdoff']

        delta = current - lastseen
        delta = delta.seconds * 1000 + delta.microseconds / 1000
        if delta < holdoff:
            return True
        return False

    def publish(self, userid, color, comment, msgid=None):
        if len(comment) > config['comment']['maxlength']:
            return TsukkomiResult.TOOLONG
        current = utc_datetime()
        if self.holdoff(userid, current):
            return TsukkomiResult.HOLDOFF
        result = tsukkomi_back.publish(
            self._ident, userid, color, comment, msgid)
        update = {'$set': {'lastseen': utc_datetime()}}
        retval = TsukkomiResult.SUCCESS
        if result:  # blacklist hit
            update['$inc'] = {'blacklist_hit': 1}
            retval = TsukkomiResult.BLACKLIST
        self._coll.find_one_and_update({'_id': userid}, update)
        return retval


class WechatUserData(BaseUserData):

    _ident = 'wechat'

    def register(self, userid, code):
        # For wechat, userid is determined when user connects
        if code != config['field']['code']:
            return False

        self._coll.update_one(
            {'_id': userid, 'source': self._ident},
            {'$set': {'verified': True, 'color': 'ffffff'}},
            upsert=True)
        return True

    def getcolor(self, userid):
        result = self._coll.find_one(
            {'_id': userid},
            projection={'color': True})
        if result:
            return result['color']
        else:
            return 'ffffff'

    def setcolor(self, userid, color):
        if not isRGBcolor(color):
            return False
        self._coll.find_one_and_update(
            {'_id': userid},
            {'$set': {'color': color}})
        return True


class WebsiteUserData(BaseUserData):

    _ident = 'website'

    def register(self, code):
        # Generate userid for user
        if code != config['field']['code']:
            return False

        while True:
            userid = random_userid()
            try:
                self._coll.insert_one({
                    '_id': userid,
                    'source': self._ident,
                    'verified': True,
                })
                break
            except DuplicateKeyError:
                continue
        return userid
