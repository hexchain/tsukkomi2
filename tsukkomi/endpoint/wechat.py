# vim: fileencoding=utf-8:

from __future__ import print_function, absolute_import, unicode_literals

from bottle import abort, route, request, response, template
from lxml import etree
import logging

from ..backend.userdata import WechatUserData
from ..utils.wechat import wechat_authenticate, value_of
from ..utils.helpers import utc_timestamp, isRGBcolor
from ..dbconfig import database
from ..config import config

logger = logging.getLogger(__name__)
wechat_user = WechatUserData(database)


@route('/api/wechat', method=['GET', 'POST'])
def api_wechat():
    colors = {
        '红': 'ff0000',
        '橙': 'ff7f00',
        '黄': 'ffff00',
        '绿': '00ff00',
        '蓝': '0000ff',
        '青': '00ffff',
        '粉': 'ff00ff',
        '紫': '8b00ff',
        '黑': '000000',
        '白': 'ffffff'
    }

    response.content_type = "application/xml"

    if request.method == 'GET' and not wechat_authenticate(
        request.params.get('signature', ''),
        request.params.get('timestamp', ''),
        request.params.get('nonce', ''),
        config['wechat']['token']
    ):
        abort(400)

    if request.params.get('echostr'):
        return request.params['echostr']

    retval = """<xml>
<ToUserName><![CDATA[{{!touser}}]]></ToUserName>
<FromUserName><![CDATA[{{!fromuser}}]]></FromUserName>
<CreateTime>{{time}}</CreateTime>
<MsgType><![CDATA[text]]></MsgType>
<Content><![CDATA[{{!text}}]]></Content>
</xml>"""

    # sanitize post body xml
    body = ''.join([line.decode('utf-8') for line in request.body])
    tree = etree.XML(body)

    msgtype = value_of(tree, "MsgType")
    me = value_of(tree, "ToUserName")
    userid = value_of(tree, "FromUserName")
    try:
        msgid = value_of(tree, "MsgId")
    except:
        msgid = None

    reply = {
        "fromuser": me,
        "touser": userid,
        "time": utc_timestamp(),
        "text": ""
    }

    if msgtype == "event":
        event = value_of(tree, "Event")
        if event == "subscribe":
            reply["text"] = config['text']['wechat']['welcome']

    elif msgtype == "text":
        text = value_of(tree, "Content").strip()
        slices = text.split(' ', 1)
        if slices[0] == '.h':
            reply['text'] = config['text']['wechat']['help']
        elif wechat_user.verify(userid):
            if slices[0] == '.k':
                reply['text'] = config['text']['wechat']['auth'][2]
            elif slices[0] == '.c':  # color input
                if len(slices) > 1:
                    if isRGBcolor(slices[1]):
                        color = slices[1]
                    else:
                        color = colors.get(slices[1])
                    if not color:
                        reply['text'] = config['text']['wechat']['color'][1]
                    else:
                        wechat_user.setcolor(userid, color)
                        reply['text'] = config['text']['wechat']['color'][0]
                else:
                    reply['text'] = config['text']['wechat']['invalid']
            else:  # comment
                color = wechat_user.getcolor(userid)
                result = wechat_user.publish(userid, color, text, msgid)
                reply['text'] = \
                    config['text']['wechat']['comment'][result.value]
        else:
            if slices[0] == '.k':
                if len(slices) > 1:
                    result = wechat_user.register(userid, slices[1])
                    reply['text'] = \
                        config['text']['wechat']['auth'][int(not result)]
                else:
                    reply['text'] = config['text']['wechat']['invalid']
            else:
                reply['text'] = config['text']['wechat']['welcome']
    else:
        reply['text'] = config['text']['wechat']['invalid']


    if not reply['text']:
        return "success"
    logger.debug("Returned: %s", template(retval, **reply))
    return template(retval, **reply)
