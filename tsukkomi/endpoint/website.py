# vim: fileencoding=utf-8:

from __future__ import absolute_import, print_function, unicode_literals

from bottle import view, request, response, route, redirect, static_file
import bottle
import logging
import json
import os

from ..dbconfig import database
from ..config import config
from ..utils.helpers import isRGBcolor
from ..utils.enum import TsukkomiResult
from ..backend.userdata import WebsiteUserData

logger = logging.getLogger(__name__)
web_user = WebsiteUserData(database)
bottle.TEMPLATE_PATH.insert(0, './frontend/dist/')


@route('/assets/<filename:path>', method='GET')
def static_files(filename):
    return static_file(filename,
                       os.path.join(bottle.TEMPLATE_PATH[0], 'assets'))


@route('/auth', method='GET')
@view('auth')
def page_auth():
    userid = request.cookies.get('userid')
    if userid and web_user.verify(userid):
        redirect('/', 302)
    return config['text']['webpage']


@route('/auth', method='POST')
def submit_auth():
    userid = request.cookies.get('userid')
    if userid and web_user.verify(userid):
        logger.debug("User already verified")
        redirect('/', 302)

    code = request.params.get('code').strip()
    if not code:
        logger.debug("Code not supplied")
        redirect('/auth', 302)

    userid = web_user.register(code)
    if not userid:
        logger.debug("Userid generation failed")
        redirect('/auth', 302)

    response.set_cookie(b'userid', userid)
    redirect('/', 302)


@route('/', method='GET')
@view('index')
def page_index():
    userid = request.cookies.get('userid')
    if not userid or not web_user.verify(userid):
        redirect('/auth', 302)
    return config['text']['webpage']


@route('/post', method='POST')
def submit_post():
    result = {'status': 0}
    color = request.params.get('color', '#ffffff')
    comment = request.params.get('comment')
    userid = request.cookies.get('userid')
    if not userid or not web_user.verify(userid):
        result['status'] = TsukkomiResult.NEEDAUTH
    if not comment:
        result['status'] = TsukkomiResult.EMPTY
    else:
        if not color.startswith('#') or not isRGBcolor(color):
            color = '#ffffff'
        result['status'] = web_user.publish(userid, color[1:], comment)

    result['status'] = result['status'].value
    return json.dumps(result)


@route('/', method='POST')
@view('index')
def submit_index():
    color = request.params.get('color', '#ffffff')
    comment = request.params.get('comment')
    userid = request.cookies.get('userid')
    if not userid or not web_user.verify(userid):
        redirect('/auth', 302)

    # FIXME need more response
    web_user.publish(userid, color[1:], comment)

    return config['text']['webpage']
