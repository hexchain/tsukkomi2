#!/usr/bin/env python2
# vim: fileencoding=utf-8:

from __future__ import absolute_import, print_function, unicode_literals

import base64
import io
import json
import os
import sys
from tsukkomi.config import config, _configobj
from tsukkomi.utils.helpers import recursive_set, random_code


if __name__ == '__main__':  # import/create default config

    if len(sys.argv) > 1:
        filename = sys.argv[1]
        default_config = json.load(io.open(sys.argv[1], encoding='utf-8'))
    else:
        default_config = {
            'text': {
                'webpage': {
                    'text': '',
                    'description': '',
                },
                'wechat': {}
            },
            'wechat': {},
            'user': {'holdoff': 3000},
            'comment': {'blacklist': []},
            'api': {},
            'field': {}
        }
    if not config:  # type safe check
        config = {}

    recursive_set(config, default_config)
    if not config['api'].get('key'):
        config['api']['key'] = base64.b64encode(os.urandom(18))
        print('Generated new API key:', config['api']['key'])
    if 'field' not in config:
        config['field'] = {}
    if not config['field'].get('code'):
        config['field']['code'] = random_code(5)
    _configobj.set_config(config)
