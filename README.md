# Tsukkomi 2

A realtime danmaku-style commenting system, aimed to increase interacting and engagement with field audience. Server side.

## Configuration

| Field name | Type | Default | Description |
|-|-|-|-|
| `text.webpage.title` | String | null | Title on webpage |
| `text.webpage.description` | String | null | Description on webpage |
| `text.wechat.welcome` | String | null | Wechat welcome message |
| `text.wechat.auth` | List(String) | null | Wechat user auth response <ul><li>`0`: success<li>`1`: failure</ul> |
| `text.wechat.color` | List(String) | null | Wechat user set color response <ul><li>`0`: success<li>`1`: failure</ul> |
| `text.wechat.comment` | List(String) | null | Wechat user send comment response <ul><li>`0`: success<li>`1`: failure, holdoff<li>`2`: failure, blacklist</ul> |
| `wechat.token` | String | null | Wechat API token |
| `wechat.aeskey` | String | null | _(currently not in use)_ Wechat API EncodingAESKey |
| `wechat.appid` | String | null | _(currently not in use)_ Wechat API AppID |
| `wechat.appsecret` | String | null | _(currently not in use)_ Wechat API AppSecret |
| `user.holdoff` | Integer | 3000 | Default holdoff time between comments, in milliseconds |
| `comment.blacklist` | List(String) | null | Comment blacklist |
| `comment.maxlength` | Integer | 100 | Comment max length |
| `api.key` | String | null | API pre-shared key, for streaming |
| `field.code` | String | null | Field code |

## Webpage endpoint
### POST Data format

| Field name | Type | Restriction | Description |
|-|-|-|-|
| `comment` | String | not null | Comment itself |
| `color` | String | `^#[0-9a-fA-F]{6}$` | Comment color |

### Cookies

| Field name | Type | Restriction | Description |
|-|-|-|-|
| `userid` | String | not null | Unique user id |

### Return format

JSON document.

| Field name | Type | Restriction | Description |
|-|-|-|-|
| `status` | Integer | | <ul><li>`0`: success<li>`1`: holdoff<li>`2`: blacklist<ul> |

## Data types

### User

| Field name | Type | Restriction | Description |
|-|-|-|-|
| `_id` | String | not null, unique | User's anonymous identity |
| `source` | String | not null | User source (`wechat` or `website`) |
| `verified` | Bool | not null | User is verified |
| `holdoff` | Integer | | How long should the user be hold off between comments |
| `lastseen` | Date | not null | User's last appearance time |
| `color` | String | not null | _(Wechat only)_ User's color |
| `blacklist_hit` | Integer | | _(currently not in use)_ How many times did the user hit the blacklist |

### Comment

| Field name | Type | Restriction | Description |
|-|-|-|-|
| `birth` | Date | not null | Comment time |
| `text` | String | not null | Comment itself |
| `color` | String | not null | Comment color |
| `placement` | Integer | not null | _(currently not in use)_ Comment placement, can be:<ul><li>`0` scroll leftwards, default<li>`1` stay top<li>`2` stay bottom</ul> |
| `user` | String | not null | Sender user ID |
| `blacklisted` | Bool | not null | True if the comment hits blacklist |
